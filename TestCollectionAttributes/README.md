Test Collection Attributes
==========================

Test JastAdd collection attribute semantic. 

IMPORTANT: the collection and attributes are defined only for testing the JastAdd semantic and they are not 
an example of good and efficient code to reuse in other projects.

For executing the tests:

```
gradle test
```
