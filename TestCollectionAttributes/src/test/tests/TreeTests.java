package tests;

import junit.framework.TestCase;
import AST.*;

import java.util.*;

public class TreeTests extends TestCase {
    BinaryNode root1;

    public void setUp() {
        ASTRoot astRoot = new ASTRoot();

        // The tested `root1` Tree.
        //
        // B = BinaryNode
        // L = Leaf
        // U = UnaryNode
        //
        //          s:B
        //     +----|-----+
        //     l:B        r:U
        //  +--|--+       |
        //  l:L   r:L     u:L
        //

        Leaf s_l_l = new Leaf("s.l.l");
        Leaf s_l_r = new Leaf("s.l.r");
        BinaryNode s_l = new BinaryNode("s.l", s_l_l, s_l_r);
        Leaf s_r_u = new Leaf("s.r.u");
        UnaryNode s_r = new UnaryNode("s.r", s_r_u);
        BinaryNode s = new BinaryNode("s", s_l, s_r);
        root1 = s;
        ASTRoot aroot1 = new ASTRoot(root1);
    }

    public void testAllChildrenCollection() {
        assertTrue(root1.allChildren().size() == 6);
        assertTrue(containsNode(root1.allChildren(), "s.l"));
        assertTrue(containsNode(root1.allChildren(), "s.r"));

        assertTrue(containsNode(root1.allChildren(), "s.l.l"));
        assertTrue(containsNode(root1.allChildren(), "s.l.r"));

        Tree s_l = findNode(root1, "s.l");
        assertTrue(containsNode(s_l.allChildren(), "s.l.l"));
        assertTrue(containsNode(s_l.allChildren(), "s.l.r"));

        Tree s_r = findNode(root1, "s.r");
        assertTrue(s_r.allChildren().size() == 2);

        // Each node contains itself
        Tree s_r_u = findNode(root1, "s.r.u");
        assertTrue(s_r_u.allChildren().size() == 1);
        assertTrue(containsNode(s_r_u.allChildren(), "s.r.u"));
    }

    public void testDirectChildrenCollection() {
        assertTrue(root1.tparent() == null);
        assertTrue(findNode(root1, "s.r").tparent() == root1);

        assertTrue(root1.directChildren().size() == 2);
        assertTrue(containsNode(root1.directChildren(), "s.l"));
        assertTrue(containsNode(root1.directChildren(), "s.r"));
        assertTrue(!containsNode(root1.directChildren(), "s.l.l"));
        assertTrue(!containsNode(root1.directChildren(), "s.l.r"));

        Tree s_l = findNode(root1, "s.l");
        assertTrue(containsNode(s_l.directChildren(), "s.l.l"));
        assertTrue(containsNode(s_l.directChildren(), "s.l.r"));

        Tree s_r = findNode(root1, "s.r");
        assertTrue(s_r.directChildren().size() == 1);

        Tree s_r_u = findNode(root1, "s.r.u");
        assertTrue(s_r_u.directChildren().size() == 0);
        assertTrue(!containsNode(s_r_u.directChildren(), "s.r.u"));
    }

    public void testDirectChildrenCollection2() {
        for (Tree n : root1.allChildren()) {
            assertTrue(isSameSet(n.directChildren(), n.directChildren2()));
        }
    }

    public void testAllParentsCollection() {
        assertTrue(root1.allParents().size() == 0);

        HashSet<Tree> r = new HashSet<Tree>();
        r.add(root1);
        Tree n = findNode(root1, "s.l");
        assertTrue(isSameSet(n.allParents(), r));

        r.add(n);
        n = findNode(root1, "s.l.l");
        assertTrue(isSameSet(n.allParents(), r));
    }

    public void testAllChildren2Attr() {
        assertTrue(root1.allChildren2().size() == 5);
        assertTrue(containsNode(root1.allChildren2(), "s.l"));
        assertTrue(containsNode(root1.allChildren2(), "s.r"));

        assertTrue(containsNode(root1.allChildren2(), "s.l.l"));
        assertTrue(containsNode(root1.allChildren2(), "s.l.r"));

        Tree s_l = findNode(root1, "s.l");
        assertTrue(containsNode(s_l.allChildren2(), "s.l.l"));
        assertTrue(containsNode(s_l.allChildren2(), "s.l.r"));

        Tree s_r = findNode(root1, "s.r");
        assertTrue(s_r.allChildren2().size() == 1);

        Tree s_r_u = findNode(root1, "s.r.u");
        assertTrue(s_r_u.allChildren2().size() == 0);
    }

    public void testAllChildren3Collection() {
        for (Tree n : root1.allChildren()) {
            assertTrue(isSameSet(n.allChildren2(), n.allChildren3()));
        }
    }

     public void testAllLeafsCollection() {
        assertTrue(root1.allLeafs().size() == 3);
        assertTrue(containsLeafNode(root1.allLeafs(), "s.l.l"));
        assertTrue(containsLeafNode(root1.allLeafs(), "s.l.r"));
        assertTrue(! containsLeafNode(root1.allLeafs(), "s.l"));
        assertTrue(! containsLeafNode(root1.allLeafs(), "s.r"));

        Tree s_l = findNode(root1, "s.l");
        assertTrue(containsLeafNode(s_l.allLeafs(), "s.l.l"));
        assertTrue(containsLeafNode(s_l.allLeafs(), "s.l.r"));

        Tree s_r = findNode(root1, "s.r");
        assertTrue(s_r.allLeafs().size() == 1);

        Tree s_r_u = findNode(root1, "s.r.u");
        assertTrue(s_r_u.allLeafs().size() == 0);
    }


    public boolean isSameSet(Set<Tree> s1, Set<Tree> s2) {
        return s1.containsAll(s2) && s2.containsAll(s1);
    }

    public boolean containsNode(Set<Tree> s, String n) {
        for (Tree nn : s) {
            if (nn.name().equals(n)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsLeafNode(Set<Leaf> s, String n) {
        for (Leaf nn : s) {
            if (nn.name().equals(n)) {
                return true;
            }
        }
        return false;
    }

    public Tree findNode(Tree s, String n) {
        for (Tree nn : s.allChildren()) {
            if (nn.name().equals(n)) {
                return nn;
            }
        }
        return null;
    }
}
