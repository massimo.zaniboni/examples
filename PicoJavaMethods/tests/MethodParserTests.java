package tests;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;

import junit.framework.TestCase;

import AST.PicoJavaParser;
import AST.PicoJavaScanner;
import AST.Program;

public class MethodParserTests extends TestCase {

	
	  // Method declarations
	  public void testMethodDecl() {
	    assertParseOk("{ boolean m() { } }");
	  }
	  public void testMethodDeclParam() {
	    assertParseOk("{ boolean m(boolean a) { } }");
	    assertParseOk("{ boolean m(boolean a, boolean b) { } }");
	  }
	  public void testMethodDeclBody() {
	    assertParseError("{ boolean m() { A.B.C a.b; } }");
	  }
	  
	  public void testMethodUse() {
		    assertParseError("{ boolean m() { A.B.C a.b; } boolean a; a = m();}");
		    assertParseError("{ boolean m(boolean a) { A.B.C a.b; } boolean a; a = a.m(a);}");
	  }

	  
	  
	  // utility asserts to test parser
	  
	  protected static void assertParseOk(String s) {
	    try {
	      parse(s);
	    } catch (Throwable t) {
	      fail(t.getMessage());
	    }
	  }
	  
	  protected static void assertParseError(String s) {
	    try {
	      parse(s);
	    } catch (Throwable t) {
	      return;
	    }
	    fail("Expected to find parse error in " + s);
	  }
	  
	  protected static void parse(String s) throws Throwable {
	    PicoJavaParser parser = new PicoJavaParser();
	    Reader reader = new StringReader(s);
	    PicoJavaScanner scanner = new PicoJavaScanner(new BufferedReader(reader));
	    Program p = (Program)parser.parse(scanner);
	    reader.close();
	  }


}
