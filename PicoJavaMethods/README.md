PicoJavaMethods Description
===========================

PicoJavaMethods extends PicoJava with methods.

In order to run PicoJavaMethods, the PicoJava directory must be a sibling directory to PicoJavaMethods. Eg., 

    examples/PicoJava
    examples/PicoJavaMethods

To run the tests:

    cd PicoJavaMethods
    ant test
