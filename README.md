# JastAdd examples
Repository for examples in JastAdd.

## Add new example
To add a new example **E** with API documentation (that will be published on the web page):

1. Create a new directory **E**
2. Add readme file **E/README.md** *(Markdown)*
3. Add build script **E/build.xml** *(ant)*
4. Add build target **doc** that generates API documentation to directory **E/doc**
5. Commit and push changes
6. Wait 5 minutes for the web page to be updated

To run on Jenkins:

1. Create build target **test** that generates an XML-formatted test report to directory **E/test-reports**
2. Add a line like this in the main `build.xml` (not the one in **E/**):
    `<ant antfile="E/build.xml" target="test" inheritAll="false"/>`
  
*(For inspiration, look at the PicoJava example)*

## Create zip files for the examples

The script `generate-web-files.sh` can be used to generate zip files and documentation for the examples. This script is used to publish the examples on the jastadd.org. For instance, this script can be invoked as a crontab job, and be invoked by a wrapper script as follows, which also copies the result to the web directory using rsync:

    #!/bin/sh
    cd /path/to/example-repository/
    ./generate-web-files.sh -f && rsync -va --no-perms --chmod=g+w dist/ /target/path/dist/

**Note**: `generate-web-files.sh` will do `git clean` and `git reset` to remove all local changes. So do not run this script if you have local changes that you want to save!