#!/bin/bash

# Check that ant exists
if ! type ant >/dev/null 2>&1; then
    echo "Error: ant cannot be found"
    exit
fi

if [ $# != 1 ] || ( [ $# == 1 ] && [ "$1" != "-f" ] ); then
    echo "This script will discard ALL local changes in the working tree."
    echo "Run this script with the flag -f to remove this warning."
    while true; do
    read -p "Proceed? (yes/no) " yn
        case $yn in 
            [Yy]* ) break;;
            [Nn]* ) exit 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
fi

git fetch
# already run?
if [ -d "dist" ]; then
    # Only run this script if there are new commits
    changes=`git rev-list HEAD...origin/master|wc -l`
    if [ $changes == 0 ]; then
        exit 1
    fi
fi

# we want a clean repository to avoid merge conflicts
git clean -f -d -q
git reset --hard

# merge
git merge origin/master

rm -rf dist
mkdir dist 

for f in `ls`
do
    if [ -d $f ] && [ "$f" != "tools" ] && [ "$f" != "dist" ] ; then
        mkdir "dist/$f"
        
        if [ -f "$f/build.xml" ]; then
            # generate API documentation
            (cd $f && ant doc 2> /dev/null > /dev/null)
            if [ $? == 0 ] && [ -d "$f/doc" ]; then
                cp -r "$f/doc" "dist/$f/doc"
            fi
            # remove generated files
            git clean -q -x -d -f "$f"
        fi
        
        # generate README file
        if [ -f "$f/README.md" ]; then
            ./tools/Markdown/Markdown.pl "$f/README.md" > "$f/README.html"
            cp "$f/README.md" "dist/$f/README.md"
        fi
        
        if [ -f "$f/index.html" ]; then
            cp "$f/index.html" "dist/$f/index.html"
        fi
    
        # include doc in zip file 
        if [ -d "dist/$f/doc" ]; then
            cp -r "dist/$f/doc" "$f/doc"
        fi

        # create zip file
        zip -r "dist/$f/$f.zip" "$f" > /dev/null
    fi
done

exit 0
